package dk.mehmedbasic.hexdefenseexpansionpack

import groovy.transform.TypeChecked

import java.util.zip.ZipEntry
import java.util.zip.ZipFile

/**
 * Reads a zip file.
 *
 * @author Jesenko Mehmedbasic
 * created 8/18/2015.
 */
@TypeChecked
class ZipReader {

    private URL url

    ZipReader(URL url) {
        this.url = url
    }

    /**
     * Downloads the zip file in the given URL to a temporary location and extracts the levels it may or may not contain.
     *
     * @throws Exception on any IO errors.
     */
    void extract() throws Exception {
        def tempFile = Files.tempFile(UUID.randomUUID().toString())
        copyInputStreamToFile(new BufferedInputStream(url.openStream()), tempFile)
        def zipFile = new ZipFile(tempFile)

        def enumeration = zipFile.entries()
        while (enumeration.hasMoreElements()) {
            ZipEntry entry = enumeration.nextElement()
            def stream = zipFile.getInputStream(entry)

            def destination
            if (entry.name.endsWith(".json")) {
                destination = Files.levelFile(entry.name)
            } else if (entry.name.endsWith(".png")) {
                destination = Files.pictureFile(entry.name)
            }

            if (destination) {
                copyInputStreamToFile(stream, destination)
            }
        }
    }


    private static void copyInputStreamToFile(InputStream inputStream, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();
        } catch (Exception e) {
            throw new IOException(e)
        }
    }
}
