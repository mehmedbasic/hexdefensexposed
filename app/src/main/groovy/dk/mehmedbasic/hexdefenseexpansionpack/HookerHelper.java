package dk.mehmedbasic.hexdefenseexpansionpack;

import de.robv.android.xposed.XposedHelpers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Helps with some method hooking due to Groovys not-always-pleasant way of handling varargs arrays.
 *
 * @author Jesenko Mehmedbasic
 *         created 8/15/2015.
 */
final class HookerHelper {
    /**
     * Finds and returns the Grid constructor.
     *
     * @param gridClass the grid class.
     * @return the resulting constructor.
     */
    static Constructor gridConstructor(Class gridClass) {
        return XposedHelpers.findConstructorBestMatch(gridClass, int.class, int.class);
    }
    /**
     * Finds and returns the needed Tile constructor.
     *
     * @param gridClass the grid class.
     * @param tileClass the tile class.
     * @return the resulting constructor.
     */
    static Constructor tileConstructor(Class gridClass, Class tileClass) {
        return XposedHelpers.findConstructorBestMatch(tileClass, gridClass, int.class, int.class, int.class);
    }


}
