package dk.mehmedbasic.hexdefenseexpansionpack

import android.util.Log
import groovy.transform.TypeChecked

/**
 * A logger with a tag.
 *
 * @author Jesenko Mehmedbasic
 * created 8/18/2015.
 */
@TypeChecked
class Logger {
    private static final String TAG = "HexDefenseExpansion"

    static void e(String msg, Throwable throwable) {
        Log.e(TAG, msg, throwable)
    }

    static void d(String msg) {
        Log.d(TAG, msg)
    }
}
