package dk.mehmedbasic.hexdefenseexpansionpack

import com.google.gson.Gson
import groovy.transform.TypeChecked

/**
 * The storage class holds level reading functionality.
 *
 * @author Jesenko Mehmedbasic
 * created 8/17/2015.
 */
@TypeChecked
class LevelStorage {

    public static boolean dirty = true

    static List<ModdedLevel> levels = []

    static void initializeLevels() {
        levels.clear()

        Files.initialize()


        def folder = Files.levelsFolder

        def levelsAsJson = folder.listFiles(new JsonFilter())
        if (levelsAsJson) {
            for (File input : levelsAsJson) {
                try {
                    levels.add(readLevel(input))
                } catch (Throwable throwable) {
                    Logger.e("Error reading levels", throwable)
                }
            }
        }
    }

    static List<ModdedLevel> getLevels() {
        if (levels.isEmpty()) {
            initializeLevels()
        }
        return levels
    }

    static int levelCount() {
        return getLevels().size()
    }

    static ModdedLevel getLevel(int index) {
        return getLevels().get(index)
    }

    static ModdedLevel readLevel(File file) {
        def reader = new FileReader(file)
        def readLevel = new Gson().fromJson(reader, ModdedLevel)
        readLevel.imageUrl = file.toURI().toURL()
        return readLevel
    }

    static class JsonFilter implements FilenameFilter {

        @Override
        boolean accept(File file, String fileName) {
            return fileName.endsWith(".json")
        }
    }
}
