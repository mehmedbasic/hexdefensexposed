package dk.mehmedbasic.hexdefenseexpansionpack.app;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import dk.mehmedbasic.hexdefenseexpansionpack.ModdedLevel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A list adapter
 *
 * @author Jesenko Mehmedbasic
 *         created 8/17/2015.
 */
public class LevelAdapter extends BaseAdapter {

    private List<ModdedLevel> levels = new ArrayList<ModdedLevel>();
    private Context context;

    public LevelAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return levels.size();
    }

    @Override
    public ModdedLevel getItem(int i) {
        return levels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            View view = View.inflate(context, R.layout.list_item, null);
            holder = new Holder(view);
            view.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.title.setText(getItem(position).getName());

        return holder.root;
    }

    public void clear() {
        levels.clear();
    }

    public void addAll(Collection<? extends ModdedLevel> collection) {
        levels.addAll(collection);
    }

    static class Holder {
        TextView author;
        TextView title;
        View root;

        public Holder(View root) {
            this.root = root;
            title = (TextView) root.findViewById(R.id.list_item_text);
        }
    }
}
