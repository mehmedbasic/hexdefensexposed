package dk.mehmedbasic.hexdefenseexpansionpack.app;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import dk.mehmedbasic.hexdefenseexpansionpack.Files;
import dk.mehmedbasic.hexdefenseexpansionpack.LevelStorage;
import dk.mehmedbasic.hexdefenseexpansionpack.ZipReader;

import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Files.initialize();

        final ListView listView = (ListView) findViewById(R.id.levelList);

        final LevelAdapter adapter = new LevelAdapter(this);
        adapter.addAll(LevelStorage.getLevels());

        listView.setAdapter(adapter);
        listView.invalidate();


        Button button = (Button) findViewById(R.id.refresh_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LevelStorage.dirty = true;

                adapter.clear();
                LevelStorage.initializeLevels();

                adapter.addAll(LevelStorage.getLevels());
                listView.invalidate();

            }
        });

        handleZipFile();
    }

    private void handleZipFile() {
        Uri data = getIntent().getData();
        if (data != null) {
            try {
                showToast("Attempting to install level from: " + data.toString(), Toast.LENGTH_SHORT);

                ZipReader reader = new ZipReader(new URL(data.toString()));
                reader.extract();
            } catch (MalformedURLException e) {
                showToast("Malformed url: " + e.getMessage(), Toast.LENGTH_LONG);
            } catch (Exception e) {
                showToast("An error occurred while downloading: " + e.getMessage(), Toast.LENGTH_LONG);
            }
        }
    }

    private void showToast(String text, int lengthShort) {
        Toast toast = Toast.makeText(this, text, lengthShort);
        toast.show();
    }


}
