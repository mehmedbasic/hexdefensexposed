package dk.mehmedbasic.hexdefenseexpansionpack

import java.lang.reflect.Array
import java.lang.reflect.Constructor
import java.lang.reflect.Method

/**
 * A factory for creating levels and injecting them into HexDefense.
 *
 * @author Jesenko Mehmedbasic
 * created 8/15/2015.
 */
class LevelFactory {
    // The constructor used for creating a HexDefense Tile
    private Constructor createTileConstructor
    // The constructor used for creating a HexDefense Grid
    private Constructor createGridConstructor
    // The HexDefense Tile class
    private Class tileClass

    /**
     * The current FileInputLevel object in the running HexDefense game.
     *
     * This object contains the grid and wave definitions.
     */
    private Object fileInputLevel

    LevelFactory(Object fileInputLevel, Class tileClass, Constructor createGridConstructor, Constructor createTileConstructor) {
        this.fileInputLevel = fileInputLevel
        this.tileClass = tileClass
        this.createGridConstructor = createGridConstructor
        this.createTileConstructor = createTileConstructor
    }

    void createGrid(ModdedLevel level) {
        fileInputLevel.grid = createGridConstructor.newInstance(level.width, level.height)
        int[] arguments = [level.width, level.height]
        def tiles = Array.newInstance(tileClass, arguments)

        try {
            for (int x = 0; x < level.width; x++) {
                def array = Array.get(tiles, x)
                for (int y = 0; y < level.height; y++) {
                    Array.set(array, y, createTile(level.tiles.get(level.height - y - 1).get(x), x, y))

                }
            }
        } catch (Throwable t) {
            Logger.e("Error in creating level", t)
        }
        fileInputLevel.grid.setTileGrid(tiles)
        fileInputLevel.creepWaveDefinitions = new ArrayList<String>()
        fileInputLevel.creepWaveDefinitions.addAll(intsToArray(level.creeps))
    }

    /**
     * Converts a list of lists of integer to a list of String.
     *
     * This is used to inject the creeps into the HexDefense level.
     *
     * @param input the input list.
     * @return the resulting list.
     */
    private static List<String> intsToArray(List<List<Integer>> input) {
        def list = []
        for (List<Integer> line : input) {
            def builder = new StringBuilder()
            for (Integer id : line) {
                builder.append(id).append(" ")
            }
            list.add(builder.toString().trim())
        }
        return list

    }

    /**
     * Creates a HexDefense tile.
     *
     * @param tileType the type of tile. See the README.md for a list of valid tiles.
     * @param x the x coordinate.
     * @param y the y coordinate.
     *
     * @return the newly created tile.
     */
    private Object createTile(int tileType, int x, int y) {
        return createTileConstructor.newInstance(fileInputLevel.grid, x, y, tileType)
    }

}
