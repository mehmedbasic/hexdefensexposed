package dk.mehmedbasic.hexdefenseexpansionpack

import android.os.Environment
import groovy.transform.TypeChecked

/**
 * Holds and creates the file structure for the expansion levels.
 *
 * @author Jesenko Mehmedbasic
 * created 8/18/2015.
 */
@TypeChecked
class Files {

    static File expansionRoot
    static File tempFolder
    static File levelsFolder
    static File picturesFolder

    /**
     * Initialize the directories used for the HexDefenseExpansion app.
     */
    static void initialize() {
        def directory = Environment.getExternalStorageDirectory()

        expansionRoot = new File(directory, "hexdefense-expansion")
        expansionRoot.mkdir()

        levelsFolder = new File(expansionRoot, "levels")
        levelsFolder.mkdir()

        picturesFolder = new File(expansionRoot, "pictures")
        picturesFolder.mkdir()

        tempFolder = new File(expansionRoot, "temp")
        tempFolder.mkdir()
    }

    static File tempFile(String name) {
        return new File(tempFolder, name)
    }

    static File levelFile(String name) {
        return new File(levelsFolder, name)
    }

    static File pictureFile(String name) {
        return new File(picturesFolder, name)
    }
}
