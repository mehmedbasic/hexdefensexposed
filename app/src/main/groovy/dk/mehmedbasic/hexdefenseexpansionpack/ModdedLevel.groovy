package dk.mehmedbasic.hexdefenseexpansionpack

import groovy.transform.TypeChecked

import java.security.MessageDigest

/**
 * A modded HexDefense level.
 *
 * @author Jesenko Mehmedbasic
 * created 8/15/2015.
 */
@TypeChecked
class ModdedLevel {
    /**
     * This is the list of all waves in the level.
     * After the last wave the whole thing gets repeated.
     *
     * See the README.md for more details on valid creeps.
     */
    List<List<Integer>> creeps = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
                                  [6, 6, 6, 6, 6, 6, 6, 6],
                                  [1, 1],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
                                  [6, 6, 6, 6, 6, 6, 6, 6],
                                  [2, 2],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
                                  [6, 6, 6, 6, 6, 6, 6, 6],
                                  [3, 3, 3],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                  [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
                                  [6, 6, 6, 6, 6, 6, 6, 6],
                                  [0, 5, 6, 0, 5, 6, 0, 5, 6, 4]]

    List<List<Integer>> tiles = [[0, 11, -1, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 2, 0, -1, 0, 0, -1, 0, 0, 0],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
                                 [0, 0, 4, 0, 4, 0, -1, -1, 0, 0, 2],
                                 [0, 0, 0, 4, 0, 0, -1, 0, -1, 12]]

    ModdedLevel() {
    }

    boolean allTowers

    URL imageUrl

    String authorName
    String name

    String cachedHash

    /**
     * Generates an MD5 hash of the authors name + the level name
     *
     * @return the md5 hash as string.
     */
    String generateUniqueIdentifier() {
        if (cachedHash) {
            return cachedHash
        }
        def instance = MessageDigest.getInstance("MD5")
        instance.update(authorName.getBytes("utf-8"))
        instance.update(name.getBytes("utf-8"))
        def digest = instance.digest()

        StringBuilder identifierBuilder = new StringBuilder();
        for (byte b : digest) {
            identifierBuilder.append(String.format("%02x", b & 0xff));
        }

        cachedHash = identifierBuilder.toString()
        return cachedHash
    }

    int getWidth() {
        return tiles.get(0).size()
    }

    int getHeight() {
        return tiles.size()
    }
}
