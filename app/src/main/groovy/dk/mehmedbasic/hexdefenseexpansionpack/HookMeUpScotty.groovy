package dk.mehmedbasic.hexdefenseexpansionpack

import android.app.Activity
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedHelpers
import dk.mehmedbasic.groovyxposedbridge.GroovyXposed

import java.lang.reflect.Field

/**
 * The Xposed hook
 *
 * @author Jesenko Mehmedbasic
 * created 7/6/2015.
 */
class HookMeUpScotty extends GroovyXposed {

    // Caching of some stuff to ensure reloading of levels is possible at runtime
    static List<String> defaultNames = []
    static List<Integer> defaultResIds = []
    static List<String> defaultIdentifiers = []

    static Field identifiersField
    static Field levelNamesField
    static Field levelResIdsField

    static final int defaultLevelCount = 35

    public HookMeUpScotty() {
        super("com.gotow.hexdefense")
    }

    @Override
    void handleLoadedPackage() {
        hookFileInputLevel()
        hookRecordManager()

        hookScoreLoop()
        hookScoreFormatter()


        hookLevelTowerUnlocker()
    }

    private void hookLevelTowerUnlocker() {
        def gameActivityClass = findClass("com.gotow.hexdefense.GameActivity")
        def levelClass = findClass("com.gotow.hexdefense.model.Level")
        def gameWorldClass = findClass("com.gotow.hexdefense.model.GameWorld")

        XposedHelpers.findAndHookConstructor(gameWorldClass, gameActivityClass, levelClass, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                def level = param.args[1]
                int levelIndex = level.getLevelNumber()
                if (levelIndex > 34) {
                    int newIndex = thisObject.levelIndex - HookMeUpScotty.defaultLevelCount
                    def moddedLevel = LevelStorage.getLevel(newIndex)

                    if (moddedLevel.allTowers) {
                        param.thisObject.towerTypeCount = 5
                    }
                }
            }
        })
    }

    private void hookScoreLoop() {
        // Extend the max modes in the scoreloop client to have all levels+our custom levels
        replaceMethod("com.scoreloop.client.android.core.model.Game") {
            method "getMaxMode"
            replace {
                def i = HookMeUpScotty.defaultLevelCount + LevelStorage.levelCount()
                result = i
            }
        }
    }

    private void hookFileInputLevel() {
        def fileInputLevelClass = findClass("com.gotow.hexdefense.model.FileInputLevel")

        def tileClass = findClass("com.gotow.hexdefense.model.Tile")
        def gridClass = findClass("com.gotow.hexdefense.model.Grid")

        def gridConstructor = HookerHelper.gridConstructor(gridClass)
        def tileConstructor = HookerHelper.tileConstructor(gridClass, tileClass)

        levelNamesField = fileInputLevelClass.declaredFields.find { it.name == "levelNames" }
        levelResIdsField = fileInputLevelClass.declaredFields.find { it.name == "levelResIds" }

        levelNamesField.accessible = true
        levelResIdsField.accessible = true

        // When populating level arrays, inject our new levels
        hookMethod(fileInputLevelClass) {
            method "populateLevelArrays"
            after {
                Logger.d("Populating levels")
                HookMeUpScotty.hookLevels()
            }
        }

        // If the level storage is dirty, force a reload
        hookMethod(fileInputLevelClass) {
            method "getLevelNames"
            before {
                if (LevelStorage.dirty) {
                    HookMeUpScotty.levelNamesField.set(null, null)
                }
            }
        }

        // If the level storage is dirty, force a reload
        hookMethod(fileInputLevelClass) {
            method "getLevelResIds"
            before {
                if (LevelStorage.dirty) {
                    HookMeUpScotty.levelResIdsField.set(null, null)
                }
            }
        }

        // This hook creates all the custom levels
        hookMethod(fileInputLevelClass) {
            method "loadFromFile"
            before {
                if (thisObject.levelIndex > 34) {
                    def factory = new LevelFactory(thisObject, tileClass, gridConstructor, tileConstructor)

                    int newIndex = thisObject.levelIndex - HookMeUpScotty.defaultLevelCount
                    factory.createGrid(LevelStorage.getLevel(newIndex))
                    result = true
                }
            }
        }

    }

    private void hookRecordManager() {
        def recordManager = findClass("com.gotow.hexdefense.model.UserRecordManager")
        def levelsField = recordManager.declaredFields.find { it.name == "levels" }

        // Unlock all custom levels
        hookMethod(recordManager) {
            method "load"
            params Activity.class
            after { Activity context ->
                ArrayList listOfLevels = levelsField.get(null) as ArrayList

                for (int i = 0; i < listOfLevels.size(); i++) {
                    if (i > 34) {
                        Object level = listOfLevels.get(i);
                        level.locked = false
                    }
                }
            }
        }
        identifiersField = recordManager.declaredFields.find { it.name == "gameItemIdentifiers" }

    }

    /**
     * The score system needs names for all levels.
     * The game supplies names for all existing levels, we have to inject names for our custom levels.
     */
    private void hookScoreFormatter() {
        def scoreFormatter = findClass("com.scoreloop.client.android.core.model.ScoreFormatter")
        Object[] objects = [int, int, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
                int newLength = HookMeUpScotty.defaultLevelCount + LevelStorage.levelCount()

                String[] modeNames = new String[newLength]
                for (int i = 0; i < HookMeUpScotty.defaultLevelCount; i++) {
                    modeNames[i] = param.result[i];
                }

                int j = 0
                for (ModdedLevel level : LevelStorage.levels) {
                    int newIndex = HookMeUpScotty.defaultLevelCount + j
                    modeNames[newIndex] = level.name
                    j++
                }

                for (int i = 0; i < modeNames.length; i++) {
                    String name = modeNames[i];
                    if (name == null) {
                        Logger.d("Index $i was null")
                    }
                }
                Logger.d("Returning mode names count=${modeNames.length}")

                param.result = modeNames
            }
        }]
        // Edge case of the GroovyXposedBridge, can't yet manipulate the result.
        XposedHelpers.findAndHookMethod(scoreFormatter, "getDefinedModesNames", objects)
    }

    /**
     * This method hook injects the levels.
     */
    static void hookLevels() {
        LevelStorage.initializeLevels()

        ArrayList<String> levelNames = levelNamesField.get(null) as ArrayList<String>
        ArrayList<Integer> levelResId = levelResIdsField.get(null) as ArrayList<Integer>

        // If our defaults are empty, this is the first time HexDefense has been run.
        // Save the default levels, so we don't break anything.
        if (defaultNames.isEmpty()) {
            defaultNames.addAll(levelNames)
            defaultResIds.addAll(levelResId)

            String[] array = identifiersField.get(null)
            Collections.addAll(defaultIdentifiers, array)
        }

        levelNames.clear()
        levelNames.addAll(defaultNames)


        levelResId.clear()
        levelResId.addAll(defaultResIds)


        int newSize = defaultIdentifiers.size() + LevelStorage.levelCount()

        String[] newArray = new String[newSize]
        for (int i = 0; i < defaultIdentifiers.size(); i++) {
            newArray[i] = defaultIdentifiers.get(i)
        }

        for (int i = 0; i < LevelStorage.levels.size(); i++) {
            ModdedLevel moddedLevel = LevelStorage.levels.get(i);
            newArray[defaultIdentifiers.size() + i] = moddedLevel.generateUniqueIdentifier()

            levelNames.add(moddedLevel.name)
            levelResId.add(-1)
        }

        identifiersField.set(null, newArray)

        LevelStorage.dirty = false
    }
}
