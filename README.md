# HexDefense Xposed #

This is a module for the Xposed Framework that adds custom level capability to [HexDefense](http://www.hexdefense.com/), a tower defense game created by Ben Gotow, Paul Caravelli, and Tony Zhang.

This module is not in any way affiliated with the HexDefense team.

## What does it do?

The module hooks into HexDefense in strategic places, enabling the user to add new levels.

The levels can be created almost freely, and the creator can even manipulate what types of waves the level will have.

## How do I use it?

Install the application and enable it in Xposed Framework.

After a reboot the module will scan `/sdcard/hexdefense-expansion/levels` for `.json` files.

## What do the JSON levels look like?

This is an example you can use:

```json
{
    "tiles": [
        [0, 11, -1, 0, 0, 0, 0, 0, 0, 0],
        [0, 2, 0, -1, 0, 0, -1, 0, 0, 0],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 0, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 2, 0, -1, 2, 0, -1, 0, 0, 2],
        [0, 0, 4, 0, 4, 0, -1, -1, 0, 0, 2],
        [0, 0, 0, 4, 0, 0, -1, 0, -1, 12]
    ],
    "creeps": [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
        [6, 6, 6, 6, 6, 6, 6, 6],
        [1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
        [6, 6, 6, 6, 6, 6, 6, 6],
        [2],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
        [6, 6, 6, 6, 6, 6, 6, 6],
        [3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
        [6, 6, 6, 6, 6, 6, 6, 6],
        [0, 5, 6, 0, 5, 6, 0, 5, 6, 4]
    ],
    "allTowers": true,
    "authorName": "Jesenko Mehmedbasic",
    "name": "You can't lose this level'"
}
```

The tiles consist of a two-dimensional array of integers, that define the given tile type.

### Tile types

The table below shows the list of legal tile types.

| Tile type | Number |
|:----------|:-----------:
| Wall | -1
| Normal tile | 0
| Healing tile (plus tile) | 1
| Damaging tile (minus tile) | 2
| Fast tile | 3 |
| Slow tile | 4 |
| Directional tile north | 5
| Directional tile north-east | 6
| Directional tile north-west | 7
| Directional tile south | 8 |
| Directional tile south-east | 9
| Directional tile south-west | 10
| Spawn tile | 11
| Goal tile | 12
| Portal A tile | 13
| Portal B tile | 14
| Portal C tile | 15

### Creeps and waves

The list of creeps in the JSON denotes the waves of the level. You can modify this list as you wish.

The following list shows the list of all valid creeps.

| Creep type | Number |
|-----------|:-----------:
| Red Square Creep | 0
| Laser Boss Creep | 1
| Minigun Boss Creep | 2
| Rocket Boss Creep | 3 |
| Shockwave boss Creep | 4 |
| Yellow Triangle Creep | 5
| Purple Cell Creep | 6

### Towers

The boolean value `allTowers` denotes whether or not the level uses all towers.

If this is set to true, the player can use the lightning towers.